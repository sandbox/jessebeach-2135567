(function ($) {

  Drupal.behaviors['dev-toolbar'] = {
    attach: function (context) {
      var $trigger = $('#dev-toolbar-popup-trigger').once('dev-toolbar');
      if ($trigger.length) {
        $trigger.on('click', function (event) {

          $('#dev-toolbar-popup').toggle();

          event.preventDefault();
        });
      }
    }
  };
}(jQuery));
